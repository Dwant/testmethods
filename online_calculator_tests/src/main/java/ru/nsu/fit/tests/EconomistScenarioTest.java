package ru.nsu.fit.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Economist Testing Scenario")
public class EconomistScenarioTest extends TestScenario {
    @BeforeClass
    void openCalculatorPage() {
        openPage();
    }

    @AfterMethod
    void clearInputElement() {
        performClearButtonClick();
    }

    @Test(groups = {"economist"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate expression with one braces pair")
    @Description("Check that Online Calculator right calculates expression with braces (input case - keyboard)," +
            "check division and multiplication correctness")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Multiplication", "Division", "Braces"})
    public void expressionWithOneBracesPair() {
        String expression = "(1000 * 0.1 / 1200) * 100";
        String expectedValue = "8.333333333333333";
        calculateExpression(expression, expectedValue);
    }

    @Test(groups = {"economist"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate expression with many braces and operations")
    @Description("Check that Online Calculator right calculates expression with braces (input case - buttons)," +
            "check operations order correctness")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Subtraction", "Multiplication", "Division", "Braces", "Unary Minus"})
    public void complexExpression() {
        String expression = "-(((1 + 2*3 - 12 + 27/9) * (10 - (2 + 100) + 347) * 2) / 102)";
        String expectedValue = "10";
        calculateExpression(expression, expectedValue);
    }

    @Test(groups = {"economist"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate floating point expression")
    @Description("Check that Online Calculator right calculates floating point expressions")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Subtraction", "Multiplication"})
    public void calculateFloatingPointExpressions() {
        calculateExpression("0.51984 - 0.2389", "0.28094");
        performClearButtonClick();
        calculateExpression("-0.51984 + 0.2389", " -0.28094");
        performClearButtonClick();
        calculateExpression("0.51984 * 0.2389", "0.124189776");
        performClearButtonClick();
        calculateExpression("0.51984 * -0.2389", "-0.124189776");
    }
}
