package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by icegu on 02.11.2015.
 */

public class SendEmptyFeedBackFormScenarioTest extends WolframBaseTestScenario {
    protected static By CONTACT_HREF = By.xpath("/html/body/footer/ul[1]/li[11]/a");
    protected static String CONTACT_URL = "http://www.wolframalpha.com/contact.html";


    @Test(groups = {"feedback-form"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Feedback form")
    @Description("Check that we can't send empty feedbackform")
    @Severity(SeverityLevel.NORMAL)
    @Features("Wolfram Feedback Form")
    public void loginFromWolframLandingPage() {
        openPage();

        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACT_HREF));

        AllureUtils.saveTextLog("Click contact href");
        browser.getElement(CONTACT_HREF).click();
        AllureUtils.saveTextLog("Check that we are on contact page");
        Assert.assertEquals(browser.getWebDriver().getCurrentUrl(), CONTACT_URL);
        AllureUtils.saveImageAttach("Contact page", browser.makeScreenshot());

        wait.until(ExpectedConditions.elementToBeClickable(FEEDBACK_FORM_HREF));

        AllureUtils.saveTextLog(String.format("Click %s", FEEDBACK_FORM_HREF));
        browser.getElement(FEEDBACK_FORM_HREF).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(FEEDBACK_FORM_DIV));

        Assert.assertTrue(browser.getElement(FEEDBACK_FORM_DIV).isDisplayed());
        Assert.assertTrue(browser.getValue(FEEDBACK_FORM_NAME).isEmpty());
        Assert.assertTrue(browser.getValue(FEEDBACK_FORM_MSG).isEmpty());
        AllureUtils.saveImageAttach("Feedback form displayed", browser.makeScreenshot());


        AllureUtils.saveTextLog(String.format("Click %s", FEEDBACK_SEND));
        browser.getElement(FEEDBACK_SEND).click();
        Assert.assertTrue(browser.getElement(FEEDBACK_FORM_DIV).isDisplayed());
        AllureUtils.saveImageAttach("Error get on submitting empty form", browser.makeScreenshot());


        AllureUtils.saveTextLog(String.format("Click %s", FEEDBACKFORM_CLOSE));
        browser.getElement(FEEDBACKFORM_CLOSE).click();
        Assert.assertFalse(browser.getElement(FEEDBACK_FORM_DIV).isDisplayed());

        AllureUtils.saveTextLog("Return to landing page");
        AllureUtils.saveTextLog(String.format("Click %s", WOLFRAM_LOGO));
        browser.getElement(WOLFRAM_LOGO).click();
        Assert.assertEquals(browser.getWebDriver().getCurrentUrl(), PAGE_URL);
        AllureUtils.saveImageAttach("Returned to landing page successfully", browser.makeScreenshot());
    }
}
