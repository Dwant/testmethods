package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by icegu on 01.11.2015.
 */
@Title("LoginLogoutScenarioTest")
public class LoginLogoutScenarioTest extends  WolframBaseTestScenario {

    @Test(/*dependsOnGroups = {"wolfram-base"}*/)
    @Title("Login test")
    @Description("Check that we can login from wolfram landing")
    @Severity(SeverityLevel.NORMAL)
    @Features("Wolfram login")
    public void loginFromWolframLandingPage() {
        openPage();

        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.elementToBeClickable(LOGIN_HREF));

        AllureUtils.saveTextLog("Click log in");
        browser.click(LOGIN_HREF);
        wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(AUTH_POPUP_FRAME));

        AllureUtils.saveTextLog("Fill wolfram ID: " + WOLFRAM_ID);
        WebDriver driver = browser.getWebDriver();
        driver.findElement(USERNAME_INPUT).sendKeys(WOLFRAM_ID);

        Assert.assertEquals(browser.getValue(USERNAME_INPUT), WOLFRAM_ID);
        AllureUtils.saveImageAttach("Fill wolfram ID: " + WOLFRAM_ID, browser.makeScreenshot());

        AllureUtils.saveTextLog("Fill password: " + PASSWORD);
        browser.typeText(PASSWORD_INPUT, PASSWORD);
        Assert.assertEquals(browser.getValue(PASSWORD_INPUT), PASSWORD);
        AllureUtils.saveImageAttach("Fill password: " + PASSWORD, browser.makeScreenshot());


        AllureUtils.saveTextLog("Click sign in");
        browser.getElement(SIGN_IN_BUTTON).click();
        AllureUtils.saveTextLog("Check if username appeared");

        driver.switchTo().defaultContent();
        wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(USERNAME_FIELD));
        Assert.assertEquals(browser.getElement(USERNAME_FIELD).getText(), WOLFRAM_ID);
        AllureUtils.saveImageAttach("Successfully logged in: " + WOLFRAM_ID, browser.makeScreenshot());

        //Open menu
        AllureUtils.saveTextLog("Open user menu");
        browser.getElement(USERNAME_FIELD).click();
        Assert.assertTrue(browser.getElement(MENU_DIV).isDisplayed());
        AllureUtils.saveTextLog("Menu opened");
        AllureUtils.saveImageAttach("Menu opened", browser.makeScreenshot());


        AllureUtils.saveTextLog("Click logout href");
        wait.until(ExpectedConditions.elementToBeClickable(LOGOUT_HREF));
        //browser.waitForElement(LOGOUT_HREF);
        browser.getElement(LOGOUT_HREF).click();

        wait.until(ExpectedConditions.elementToBeClickable(LOGIN_HREF));
        Assert.assertTrue(browser.getElement(LOGIN_HREF).isDisplayed());
        AllureUtils.saveImageAttach("Successfully logged out", browser.makeScreenshot());
    }

    private void login() {

    }

    private void logout() {

    }


}
