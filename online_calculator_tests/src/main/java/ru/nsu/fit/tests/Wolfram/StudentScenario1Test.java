package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.TestScenario;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Student Testing Scenario 1 (Trigonometry)")
public class StudentScenario1Test extends WolframBaseTestScenario {
    @BeforeClass
    void openPageAndWaitForLoad() {
        openPage();
        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACT_HREF));
    }

    @BeforeMethod
    void clearInput() {
        clearInputField();
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate 'sin' expressions")
    @Description("Check that Online Calculator right calculates 'sin' function")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateSin() {
        calculateExpression("sin(0)", "0");
        calculateExpression("sin(pi/2)", "1");
        calculateExpression("sin(pi)", "0");
        calculateExpression("sin(3pi/2)", "-1");
        calculateExpression("sin(2pi)", "0");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate 'cos' expressions")
    @Description("Check that Online Calculator right calculates 'cos' function")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateCos() {
        calculateExpression("cos(0)", "1");
        calculateExpression("cos(pi/2)", "0");
        calculateExpression("cos(pi)", "-1");
        calculateExpression("cos(3pi/2)", "0");
        calculateExpression("cos(2pi)", "1");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate 'tg' expressions")
    @Description("Check that Online Calculator right calculates 'tg' function")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateTg() {
        calculateExpression("tg(0)", "0");
        calculateExpression("tg(pi/4)", "1");
        calculateExpression("tg(pi/2)", "infinity^~");
        calculateExpression("tg(pi)", "0");
        calculateExpression("ctg(3pi/4)", "-1");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate 'ctg' expressions")
    @Description("Check that Online Calculator right calculates 'ctg' function")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateCtg() {
        calculateExpression("ctg(0)", "infinity^~");
        calculateExpression("ctg(pi/4)", "1");
        calculateExpression("ctg(pi/2)", "0");
        calculateExpression("ctg(pi)", "infinity^~");
        calculateExpression("ctg(3pi/4)", "-1");
    }
}
