package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by icegu on 01.11.2015.
 */
public class WolframBaseTestScenario {
    public static String PAGE_URL = "http://www.wolframalpha.com/";

    protected static String WOLFRAM_ID = "iceguardian@mail.ru";
    protected static String PASSWORD = "testmethods";
    protected static By LOGIN_HREF = By.xpath("//*[@id=\"oauth_signin\"]/div[2]");
    protected static By AUTH_POPUP_FRAME = By.xpath("//*[@id=\"oauth_signin_iframe\"]");
    protected static String AUTH_FRAME_ID = "oauth_signin_iframe";
    protected static By USERNAME_INPUT = By.xpath("//*[@id=\"username\"]");
    protected static By PASSWORD_INPUT =  By.xpath("//*[@id=\"password\"]");
    protected static By SIGN_IN_BUTTON = By.xpath("//*[@id=\"submitSignIn\"]");
    protected static By USERNAME_FIELD = By.xpath("//*[@id=\"oauth_signin\"]/span");
    protected static By MENU_DIV = By.xpath("//*[@id=\"authbox\"]");
    protected static By LOGOUT_HREF = By.xpath("//*[@id=\"authbox-signout-link\"]");


    //feedbackform
    protected static By CONTACT_HREF = By.xpath("/html/body/footer/ul[1]/li[11]/a");
    protected static String CONTACT_URL = "http://www.wolframalpha.com/contact.html";
    protected static By FEEDBACK_FORM_HREF = By.xpath("//*[@id=\"feedbackLink\"]");
    protected static By FEEDBACK_FORM_DIV = By.xpath("//*[@id=\"lboxContent_feedbackForm\"]");
    protected static By FEEDBACK_SEND = By.xpath("//*[@id=\"sendImg\"]");
    protected static By FEEDBACK_FORM_NAME = By.xpath("//*[@id=\"name\"]");
    protected static By FEEDBACK_FORM_MSG = By.xpath("//*[@id=\"message\"]");
    protected static By FEEDBACKFORM_CLOSE = By.xpath("//*[@id=\"feedbackFormContain\"]/div[1]/div[1]/a");
    protected static By WOLFRAM_LOGO = By.xpath("//*[@id=\"waLogo\"]");

    //tasks
    protected static By TASK_INPUT = By.xpath("//*[@id=\"i\"]");
    protected static By CALCULATE_BUTTON = By.xpath("//*[@id=\"equal\"]");
    protected static By ANSWER_SECTION = By.xpath("//*[@id=\"answers\"]");
    protected static By RESULT_AREA = By.xpath("//*[@id=\"scannerresult_0200_1\"]");
    protected static By SHOW_PLAIN_TEXT_ANSWER = By.xpath("//*[@id=\"subpod_0200_1\"]/div/div[2]/button[4]");
    protected static By PLAIN_TEXT_ANSWER_AREA = By.xpath("//*[@id=\"mov_subpod_0200_1_popup_dyn\"]/pre[1]");
    protected static By WOLFRAM_LANG_TEXT_ANSWER_AREA = By.xpath("//*[@id=\"mov_subpod_0200_1_popup_dyn\"]/pre[2]");

    protected Browser browser;

    @BeforeClass()
    public void openBrowser() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass()
    public void closeBrowser() {
        browser.close();
    }

    public void openPage() {
        AllureUtils.saveTextLog("Open page");
        browser.openPage(PAGE_URL);
        AllureUtils.saveTextLog("The page was opened successfully");
        AllureUtils.saveImageAttach("Main screen", browser.makeScreenshot());
    }

    public void clearInputField() {
        browser.clearElement(TASK_INPUT);
    }

    public void calculateExpression(String expression, String expectedResult) {
        calculate(expression);
        checkPlainText(expectedResult);
        browser.clearElement(TASK_INPUT);
    }

    public void calculateExpressionWithWolframLangOutput(String expression, String expectedResult) {
        calculate(expression);
        checkWolframLangText(expectedResult);
        browser.clearElement(TASK_INPUT);
    }

    private void calculate(String expression) {
        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);

        AllureUtils.saveTextLog(String.format("Fill input with task: %s", expression));
        browser.getElement(TASK_INPUT).sendKeys(expression);
        Assert.assertEquals(browser.getValue(TASK_INPUT), expression);
        AllureUtils.saveImageAttach("Filled task", browser.makeScreenshot());

        AllureUtils.saveTextLog("Calculate click");
        browser.getElement(CALCULATE_BUTTON).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(ANSWER_SECTION));
        Assert.assertTrue(browser.getElement(ANSWER_SECTION).isDisplayed());
        AllureUtils.saveImageAttach("Result calculated", browser.makeScreenshot());

        AllureUtils.saveTextLog("Focus result area");
        new Actions(browser.getWebDriver()).moveToElement(browser.getElement(RESULT_AREA)).perform();
        Assert.assertTrue(browser.getElement(SHOW_PLAIN_TEXT_ANSWER).isDisplayed());
        AllureUtils.saveImageAttach("Result area focused", browser.makeScreenshot());

        wait.until(ExpectedConditions.visibilityOfElementLocated(SHOW_PLAIN_TEXT_ANSWER));
    }

    private void checkPlainText(String expectedResult) {
        AllureUtils.saveTextLog("Click to get plaintext result");
        browser.getElement(SHOW_PLAIN_TEXT_ANSWER).click();
        Assert.assertEquals(browser.getElement(PLAIN_TEXT_ANSWER_AREA).getText(), expectedResult);
        AllureUtils.saveImageAttach("Plaintext result", browser.makeScreenshot());
    }

    private void checkWolframLangText(String expectedResult) {
        AllureUtils.saveTextLog("Click to get plaintext result");
        browser.getElement(SHOW_PLAIN_TEXT_ANSWER).click();
        Assert.assertEquals(browser.getElement(WOLFRAM_LANG_TEXT_ANSWER_AREA).getText(), expectedResult);
        AllureUtils.saveImageAttach("Plaintext result", browser.makeScreenshot());
    }
}
