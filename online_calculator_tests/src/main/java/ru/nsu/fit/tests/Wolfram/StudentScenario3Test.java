package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Maxim Vakhrushev on 03.11.2015.
 */
@Title("Student Testing Scenario 3 (Discrete Maths)")
public class StudentScenario3Test extends WolframBaseTestScenario {
    @BeforeClass
    void openPageAndWaitForLoad() {
        openPage();
        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACT_HREF));
    }

    @BeforeMethod
    void clearInput() {
        clearInputField();
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate matrix operations")
    @Description("Check that Online Calculator right calculates matrix operations")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Matrix Algebra"})
    public void calculateMatrixOperation() {
        calculateExpressionWithWolframLangOutput("{{0,-1},{1,0}}*{{1,2},{3,4}}+{{2,-1},{-1,2}}", "{{-1, -5}, {0, 4}}");
        calculateExpression("{{0,-1, 1,0}}*{{1},{2},{3},{4}}", "(1)");
        calculateExpressionWithWolframLangOutput("{{0,-1, 1,0}}*{{1},{2},{3},{4}}", "{{1}}");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate combinatorics expressions")
    @Description("Check that Online Calculator right calculates combinatorics expressions")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Combinatorics"})
    public void calculateCombinatorics() {
        calculateExpression("30 choose 18", "86493225");
        calculateExpression("20 choose 30", "0");
        calculateExpression("-20 choose 30", "18851684897584");
        calculateExpression("-20 choose -30", "20030010");
        calculateExpression("-20 choose -30", "20030010");
    }
}
