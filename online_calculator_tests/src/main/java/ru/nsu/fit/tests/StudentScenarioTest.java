package ru.nsu.fit.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Student Testing Scenario 2")
public class StudentScenarioTest extends TestScenario{
    @BeforeClass
    void openCalculatorPage() {
        openPage();
    }

    @AfterMethod
    void clearInputElement() {
        performClearButtonClick();
    }

    @Test(groups = {"student"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate trigonometry expressions")
    @Description("Check that Online Calculator right calculates trigonometry expressions")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Trigonometry"})
    public void calculateTrigonomety() {
        calculateExpression("sin(0)", "0");
        performClearButtonClick();
        calculateExpression("cos(pi/2)", "0");
        performClearButtonClick();
        calculateExpression("tg(pi/4)", "1");
        performClearButtonClick();
        calculateExpression("ctg(3*(pi/2))", "-1");
        performClearButtonClick();
    }
}
