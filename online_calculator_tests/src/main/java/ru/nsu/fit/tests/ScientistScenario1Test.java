package ru.nsu.fit.tests;

import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Scientist Testing Scenario 1")
public class ScientistScenario1Test extends TestScenario {
    @Test(groups = {"scientist"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate expression with scientific notation numbers and continuous input")
    @Description("Check that Online Calculator right calculates expression with scientific notation numbers" +
            "(input case - keyboard), check continuous input correctness")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Subtraction", "Multiplication", "Division", "Braces",
            "Scientific Notation", "Continuous Keyboard Input"})
    public void scientificNotationExpressions() {
        openPage();

        calculateExpression("(2.5e-3 * 1e4) / 1e-7 - 987654321", "-737654321");
        performClearButtonClick();
        calculateExpression("1 / 10000000", "1e-7");
        calculateExpression("* 10000000", "1");
        calculateExpression("+ 2e1", "21");
        calculateExpression("/ 1e1", "2.1");
        calculateExpression("- 11e-1", "1");
        calculateExpression("* 10e0", "10");
        performClearButtonClick();
    }
}
