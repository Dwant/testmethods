package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;

import java.util.HashMap;

/**
 * Created by Maxim Vakhrushev on 27.10.2015.
 */
public class TestScenario {
    public static final String PAGE_URL = "http://testmethods.tmweb.ru/";
    public static final By inputElement = By.xpath("//input[@type='text' and @name='Input']");
    public static final By equalElement = By.xpath("//input[@type='button' and @name='DoIt']");
    public static final String[] numberNames = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    public static final HashMap<Integer, By> numberButtonsMap;
    static {
        numberButtonsMap = new HashMap<>();
        for (int i = 0; i < numberNames.length; i++) {
            numberButtonsMap.put(i, By.xpath(String.format("//input[@type='button' and @name='%s']", numberNames[i])));
        }
    }

    public static final By addElement = By.xpath("//input[@type='button' and @name='plus']");
    public static final By subElement = By.xpath("//input[@type='button' and @name='minus']");
    public static final By mulElement = By.xpath("//input[@type='button' and @name='times']");
    public static final By divElement = By.xpath("//input[@type='button' and @name='div']");
    public static final By clearElement = By.xpath("//input[@type='button' and @name='clear']");

    enum Operation {
        ADD, SUB, MUL, DIV
    }

    private static final HashMap<Operation, String> operationsAbbreviations;
    private static final HashMap<Operation, By> operationsUIElements;
    private static final HashMap<Operation, String> operationsSymbols;
    static {
        operationsAbbreviations = new HashMap<>();
        operationsAbbreviations.put(Operation.ADD, "Add");
        operationsAbbreviations.put(Operation.SUB, "Sub");
        operationsAbbreviations.put(Operation.MUL, "Mul");
        operationsAbbreviations.put(Operation.DIV, "Div");

        operationsUIElements = new HashMap<>();
        operationsUIElements.put(Operation.ADD, addElement);
        operationsUIElements.put(Operation.SUB, subElement);
        operationsUIElements.put(Operation.MUL, mulElement);
        operationsUIElements.put(Operation.DIV, divElement);

        operationsSymbols = new HashMap<>();
        operationsSymbols.put(Operation.ADD, " + ");
        operationsSymbols.put(Operation.SUB, " - ");
        operationsSymbols.put(Operation.MUL, " * ");
        operationsSymbols.put(Operation.DIV, " / ");
    }

    protected Browser browser;

    @BeforeClass()
    public void openBrowser() {
        browser = BrowserService.openNewBrowser();
    }

    @AfterClass()
    public void closeBrowser() {
        browser.close();
    }

    public void openPage() {
        AllureUtils.saveTextLog("Open page");
        browser.openPage(PAGE_URL);
        AllureUtils.saveTextLog("The page was opened successfully");
        AllureUtils.saveImageAttach("Main screen", browser.makeScreenshot());
    }

    protected void enterNumberByButtons(String number) {
        AllureUtils.saveTextLog(String.format("Entering number %s", number));
        String inputHistory = browser.getValue(AcceptanceTest.inputElement);

        for (int i = 0; i < number.length(); i++) {
            int chiffre = Integer.parseInt(number.substring(i, i + 1));
            browser.click(TestScenario.numberButtonsMap.get(chiffre));
        }
        Assert.assertEquals(browser.getValue(TestScenario.inputElement), inputHistory + number);
        AllureUtils.saveTextLog(String.format("Number %s is correct", number));
        AllureUtils.saveImageAttach(String.format("Number %s", number), browser.makeScreenshot());
    }

    protected StringBuilder getCurrentInput() {
        return new StringBuilder().append(browser.getValue(TestScenario.inputElement));
    }

    protected void performOperationButtonClick(Operation op) {
        StringBuilder prevInputFieldValue = getCurrentInput();

        AllureUtils.saveTextLog(operationsAbbreviations.get(op) + " Operation button click ");
        browser.click(operationsUIElements.get(op));

        Assert.assertEquals(browser.getValue(inputElement), prevInputFieldValue + operationsSymbols.get(op));
        AllureUtils.saveTextLog(operationsAbbreviations.get(op) + " operation button clicked");
        AllureUtils.saveImageAttach(operationsAbbreviations.get(op) + " operation", browser.makeScreenshot());
    }

    protected void performClearButtonClick() {
        AllureUtils.saveTextLog("Clear Operation button click ");
        browser.click(clearElement);
        Assert.assertEquals(browser.getValue(inputElement), "");
        AllureUtils.saveTextLog("Clear operation button clicked");
        AllureUtils.saveImageAttach("Clear operation", browser.makeScreenshot());
    }

    protected void performEqualButtonClick(String expectedResult) {

        StringBuilder prevInputFieldValue = getCurrentInput();

        if (expectedResult == null) {
            // TODO: throw new InvalidDataException();
        }
        AllureUtils.saveTextLog("Equals button click ");
        browser.click(equalElement);
//        if (prevInputFieldValue != null) {
//            Assert.assertEquals(browser.getValue(inputElement), prevInputFieldValue + "=");
//            prevInputFieldValue.append("=");
//        } else {
//            Assert.assertEquals(browser.getValue(inputElement), "=");
//        }
        Assert.assertEquals(browser.getValue(inputElement), expectedResult);
        AllureUtils.saveTextLog("Operation performed");
        AllureUtils.saveImageAttach("Right result", browser.makeScreenshot());
    }

    protected void calculateExpression(String expression, String expectedResult) {
        if (expression == null) {
            //TODO
        }
        if (expectedResult == null) {
            //TODO
        }
        browser.typeText(inputElement, expression);
        AllureUtils.saveImageAttach("The expression is typed", browser.makeScreenshot());
        browser.click(equalElement);
        AllureUtils.saveImageAttach("Result", browser.makeScreenshot());
        Assert.assertEquals(browser.getValue(inputElement), expectedResult);
    }
}
