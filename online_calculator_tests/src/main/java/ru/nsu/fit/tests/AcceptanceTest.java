package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Acceptance test")
public class AcceptanceTest extends TestScenario{
    @Test(groups = {"base"})
    @Title("Open page")
    @Description("Check that we can open page with Online Calculator")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"UI feature", "Base"})
    public void testOpenPage() {
        AllureUtils.saveTextLog("Open page");
        browser.openPage(PAGE_URL);
        AllureUtils.saveTextLog("The page was opened successfully");
        AllureUtils.saveImageAttach("Main screen", browser.makeScreenshot());
        browser.typeText(inputElement, "1");
        AllureUtils.saveImageAttach("The expression is typed", browser.makeScreenshot());
        Assert.assertEquals(browser.getValue(inputElement), "1");
        AllureUtils.saveImageAttach("The expression is correct", browser.makeScreenshot());
    }
}
