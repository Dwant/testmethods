package ru.nsu.fit.tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Hacker Testing Scenario")
public class HackerScenarioTest extends TestScenario {
    @Test(groups = {"security"})
    @Title("Execute JS 'alert'")
    @Description("Check that Online Calculator is protected from injections")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Security"})
    public void executeAlert() {
        browser.openPage(PAGE_URL);
        AllureUtils.saveImageAttach("Main screen", browser.makeScreenshot());
        browser.typeText(inputElement, "alert(\"Hello, world!\");");
        AllureUtils.saveImageAttach("The expression is typed", browser.makeScreenshot());
        browser.click(equalElement);
        try
        {
            Alert alert = browser.switchToAlert();
            if (alert != null)
            {
                //AllureUtils.saveImageAttach("Bad Result", browser.makeScreenshot());
                AllureUtils.saveTextLog("Not expected allert message", alert.getText());
                Assert.assertTrue(false);
                alert.accept();
            }
        }
        catch (NoAlertPresentException ex) {
            AllureUtils.saveImageAttach("OK Result", browser.makeScreenshot());
        }

        Assert.assertEquals(browser.getValue(inputElement), "Invalid expression");
    }
}
