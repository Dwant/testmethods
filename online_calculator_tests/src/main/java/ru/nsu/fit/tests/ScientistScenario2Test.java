package ru.nsu.fit.tests;

import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Scientist Testing Scenario 2")
public class ScientistScenario2Test extends TestScenario {
    public static final String NaN = "NaN";
    public static final String Infinity = "Infinity";
    public static final String NegInfinity = "-Infinity";

    @Test(groups = {"scientist"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate expression with Infinity and continuous input")
    @Description("Check that Online Calculator right calculates expression with Infinity (input case - buttons)," +
            "check continuous input correctness")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Subtraction", "Multiplication", "Division", "Braces",
            "Infinity", "NaN", "Continuous Keyboard Input"})
    public void infinityAndNaNExpressions() {
        openPage();

        calculateExpression("1000 / 0", Infinity);
        calculateExpression("*0", NaN);
        calculateExpression("+ 1000", NaN);
        calculateExpression("- 1000", NaN);
        calculateExpression("* 1000", NaN);
        calculateExpression("/ 1000", NaN);
        calculateExpression("+ Infinity", NaN);
        calculateExpression("- Infinity", NaN);
        calculateExpression("* Infinity", NaN);
        calculateExpression("/ Infinity", NaN);
        calculateExpression("+ NaN", NaN);
        calculateExpression("- NaN", NaN);
        calculateExpression("* NaN", NaN);
        calculateExpression("/ NaN", NaN);
        performClearButtonClick();
        calculateExpression("Infinity * (-Infinity)", NegInfinity);
        performClearButtonClick();
        calculateExpression("0 / 0", NaN);
        performClearButtonClick();
        calculateExpression("Infinity * Infinity", Infinity);
        calculateExpression("+ 1000",  Infinity);
        calculateExpression("- 1000",  Infinity);
        calculateExpression("* 1000",  Infinity);
        calculateExpression("/ 1000",  Infinity);
        calculateExpression("+ Infinity",  Infinity);
        calculateExpression("/ Infinity",  NaN);
        performClearButtonClick();
        calculateExpression("Infinity - Infinity",  NaN);
    }
}
