package ru.nsu.fit.tests.Wolfram;

import jdk.nashorn.internal.AssertsEnabled;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Student Testing Scenario 2 (Different Maths)")
public class StudentScenario2Test extends WolframBaseTestScenario {
    @BeforeClass
    void openPageAndWaitForLoad() {
        openPage();
        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACT_HREF));
    }

    @BeforeMethod
    void clearInput() {
        clearInputField();
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate involution expressions")
    @Description("Check that Online Calculator right calculates involution expressions")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateInvolution() {
        calculateExpression("e^2", "7.389056098930650227230427460575007813180315570551847324087127...");
        calculateExpression("2^2", "4");
        calculateExpression("100^0", "1");
        calculateExpression("e^Infinity", "infinity");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Convert numbers to different bases")
    @Description("Check that Online Calculator right convert numbers")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Numbers Convertation"})
    public void convertNumbers() {
        calculateExpression("219 to binary", "11011011_2");
        calculateExpression("219 to hex", "db_16");
        calculateExpression("convert 219 to base 1)", "indeterminate");
        calculateExpression("convert 219 to base 11)", "18a_11");
    }

    @Test(groups = {"student-wolfram"}/*, dependsOnGroups = {"wolfram-base"}*/)
    @Title("Calculate euler function")
    @Description("Check that Online Calculator right calculates euler function")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Wolfram Trigonometry"})
    public void calculateEulerFunction() {
        calculateExpression("phi(0)", "0");
        calculateExpression("phi(17)", "16");
        calculateExpression("phi(8)", "4");
        calculateExpression("phi(1234567890)", "329040288");
    }
}
