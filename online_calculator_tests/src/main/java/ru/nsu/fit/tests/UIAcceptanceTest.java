package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("UI acceptance test")
public class UIAcceptanceTest extends TestScenario {
    @Test(groups = { "ui" }/*, dependsOnGroups = {"base"}*/)
    @Title("Check 0-9 buttons")
    @Description("Check 0-9 buttons work")
    @Severity(SeverityLevel.BLOCKER)
    @Features("UI feature")
    public void testNumberButtons() {
        openPage();
        for (int i = 0; i <= 9; i++) {
            enterNumberByButtons(Integer.toString(i));
            Assert.assertEquals(browser.getValue(TestScenario.inputElement), Integer.toString(i));
            AllureUtils.saveImageAttach(String.format("Button \"%s\" is correct", i), browser.makeScreenshot());
            performClearButtonClick();
        }
    }

}
