package ru.nsu.fit.tests.Wolfram;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Acceptance test")
public class AcceptanceTest extends WolframBaseTestScenario {
    protected static String TASK = "pi to 10 digits";
    protected static String TASK_RESULT = "3.141592654";

    @Test(groups = {"wolfram-base"})
    @Title("Open page")
    @Description("Check that we can open wolfram landing")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Wolfram Base")
    public void openWolframLandingPage() {
        AllureUtils.saveTextLog("Open page");
        browser.openPage(PAGE_URL);
        AllureUtils.saveTextLog("The page was opened successfully");
        AllureUtils.saveImageAttach("Main screen", browser.makeScreenshot());
    }

    @Test(groups = {"wolfram-base"})
    @Title("Calculate pi")
    @Description("Check that we can get text result")
    @Severity(SeverityLevel.NORMAL)
    @Features("Wolfram Base")
    public void calculatePi() {
        openPage();
        WebDriverWait wait = new WebDriverWait(browser.getWebDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACT_HREF));
        calculateExpression(TASK, TASK_RESULT);
    }
}
