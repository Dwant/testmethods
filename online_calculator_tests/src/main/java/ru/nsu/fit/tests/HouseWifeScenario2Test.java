package ru.nsu.fit.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("HouseWife Testing Scenario 2")
public class HouseWifeScenario2Test extends TestScenario {
    @BeforeClass
    void openCalculatorPage() {
        openPage();
    }

    @BeforeMethod
    void clearInputElement() {
        performClearButtonClick();
    }

    @Test(groups = {"housewife"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate cost of purchases by UI buttons")
    @Description("Check that Online Calculator right calculates sums and multiplications (input case - buttons)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Multiplication"})
    public void countCostOfPurchases() {
        String[] purchasesCosts = {"10", "17", "3987", "1", "123", "1", "987654321"};
        String[] purchasesCounts = {"13", "39", "10", "987654321", "4", "10", "1"};
        String expectedValue = "1975349807";

        StringBuilder curInputFieldValue = new StringBuilder();
        for (int i = 0; i < purchasesCounts.length; ++i) {
            enterNumberByButtons(purchasesCounts[i]);
            curInputFieldValue.append(purchasesCounts[i]);
            performOperationButtonClick(TestScenario.Operation.MUL);
            enterNumberByButtons(purchasesCosts[i]);
            curInputFieldValue.append(purchasesCosts[i]);
            if (i != purchasesCounts.length - 1) {
                performOperationButtonClick(TestScenario.Operation.ADD);
            }
        }
        performEqualButtonClick(expectedValue);
    }

    @Test(groups = {"housewife"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate cost of purchases with Clear button click by UI buttons")
    @Description("Check that Online Calculator right calculates sums and multiplications(input case - buttons)," +
            "check correctness of clear button ")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Multiplication"})
    public void countCostOfPurchasesWithClears() {
        String[] purchasesCosts = {"10", "17", "3987", "1", "123", "1", "987654321"};
        String[] purchasesCounts = {"13", "39", "10", "987654321", "4", "10", "1"};
        String expectedValue = "1975349807";

        StringBuilder curInputFieldValue = new StringBuilder();
        for (int i = 0; i < purchasesCounts.length; ++i) {
            enterNumberByButtons(purchasesCounts[i]);
            curInputFieldValue.append(purchasesCounts[i]);
            performOperationButtonClick(TestScenario.Operation.MUL);
            enterNumberByButtons(purchasesCosts[i]);
            curInputFieldValue.append(purchasesCosts[i]);
            if (i != purchasesCounts.length - 1) {
                performOperationButtonClick(TestScenario.Operation.ADD);
            }
        }

        // Housewife thought that she made a mistake and wants to start over
        performClearButtonClick();
        curInputFieldValue = new StringBuilder();
        for (int i = 0; i < purchasesCounts.length; ++i) {
            enterNumberByButtons(purchasesCounts[i]);
            curInputFieldValue.append(purchasesCounts[i]);
            performOperationButtonClick(TestScenario.Operation.MUL);
            enterNumberByButtons(purchasesCosts[i]);
            curInputFieldValue.append(purchasesCosts[i]);
            if (i != purchasesCounts.length - 1) {
                performOperationButtonClick(TestScenario.Operation.ADD);
            }
        }
        performEqualButtonClick(expectedValue);
    }
}
