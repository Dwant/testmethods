package ru.nsu.fit.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("HouseWife Testing Scenario 1")
public class HouseWifeScenario1Test extends TestScenario {
    @BeforeClass
    void openCalculatorPage() {
        openPage();
    }

    @AfterMethod
    void clearInputElement() {
        performClearButtonClick();
    }

    @Test(groups = {"housewife"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate budget balance")
    @Description("Check that Online Calculator right calculates substitution of 2 positive integers (input case - buttons)")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Subtraction"})
    public void countBudgetBalance() {
        String startBudget = "10345";
        String wasted = "9043";

        StringBuilder curInputFieldValue = new StringBuilder();
        enterNumberByButtons(startBudget);
        curInputFieldValue.append(startBudget);
        performOperationButtonClick(Operation.SUB);
        enterNumberByButtons(wasted);
        performEqualButtonClick("1302");
    }

    @Test(groups = {"housewife"}/*, dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate average month waste during 1 year")
    @Description("Check that Online Calculator right calculates sum of several positive integers (input case - buttons)" +
            "and division correctness")
    @Severity(SeverityLevel.CRITICAL)
    @Features({"Addition", "Multiplication", "Division"})
    public void countAverageMonthWaste() {
        String[] monthsWastes = {"9346", "7749", "8912", "6987", "9456", "29670", "9876", "15432", "13900", "12346",
                "10896", "25000"};
        String sum = "159570";

        StringBuilder curInputFieldValue = new StringBuilder();
        enterNumberByButtons(monthsWastes[0]);
        curInputFieldValue.append(monthsWastes[0]);
        performOperationButtonClick(Operation.ADD);
        for (int i = 1; i < monthsWastes.length; ++i) {
            enterNumberByButtons(monthsWastes[i]);
            curInputFieldValue.append(monthsWastes[i]);
            if (i != monthsWastes.length - 1) {
                performOperationButtonClick(Operation.ADD);
            }
        }
        performEqualButtonClick(sum);
        curInputFieldValue = new StringBuilder();
        curInputFieldValue.append(sum);
        performOperationButtonClick(Operation.DIV);
        enterNumberByButtons(String.valueOf(monthsWastes.length));
        performEqualButtonClick("13297.5");
    }
}
