package ru.nsu.fit.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Pupil Testing Scenario")
public class PupilScenarioTest extends TestScenario {
    @Test(groups = {"pupil"} /*,dependsOnGroups = {"base", "ui"}*/)
    @Title("Calculate simple expressions")
    @Description("Check that Online Calculator right calculates simple expressions (input case - keyboard)")
    @Severity(SeverityLevel.BLOCKER)
    @Features({"Addition", "Subtraction", "Multiplication", "Division"})
    public void checkAddition() {
        openPage();
        calculateExpression("1+2", "3");
        performClearButtonClick();
        calculateExpression("10*2", "20");
        performClearButtonClick();
        calculateExpression("5-7", "-2");
        performClearButtonClick();
        calculateExpression("1/2", "0.5");
        performClearButtonClick();
        calculateExpression("10-2", "8");
        performClearButtonClick();
        calculateExpression("-1 + -2", "-3");
        performClearButtonClick();
        calculateExpression("-1 * -2", "2");
        performClearButtonClick();
        calculateExpression("-1 / -2", "0.5");
        performClearButtonClick();
        calculateExpression("-1 + 3", "2");
        performClearButtonClick();
        calculateExpression("-1 * 10", "-10");
        performClearButtonClick();
        calculateExpression("-1 / 10", "-0.1");
        performClearButtonClick();
    }
}
