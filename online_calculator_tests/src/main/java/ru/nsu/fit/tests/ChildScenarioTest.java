package ru.nsu.fit.tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("Child Testing Scenario")
public class ChildScenarioTest extends TestScenario {
    @BeforeClass
    void openCalculatorPage() {
        openPage();
    }

    @AfterMethod
    void clearInputElement() {
        performClearButtonClick();
    }

    @Test(groups = {"child"})
    @Title("Enter letters sequence")
    @Description("Check that Online Calculator is correctly handles invalid input (letters sequence")
    @Severity(SeverityLevel.NORMAL)
    @Features({"Invalid input"})
    public void enterLetters() {
        calculateExpression("ABCDEF", "undefined");
    }

    @Test(groups = {"child"})
    @Title("Enter empty braces")
    @Description("Check that Online Calculator is correctly handles invalid input (empty braces)")
    @Severity(SeverityLevel.NORMAL)
    @Features({"Invalid input"})
    public void enterEmptyBraces() {
        calculateExpression("((())", "undefined");
    }

    @Test(groups = {"child"})
    @Title("Enter expression with invalid braces")
    @Description("Check that Online Calculator is correctly handles invalid input (expression with invalid braces)")
    @Severity(SeverityLevel.NORMAL)
    @Features({"Invalid input"})
    public void enterExpressionWithInvalidBraces() {
        calculateExpression("(10-(2*10)", "undefined");
    }

    @Test(groups = {"child"})
    @Title("Exnter empty expression")
    @Description("Check that Online Calculator is correctly handles invalid input (empty expression)")
    @Severity(SeverityLevel.NORMAL)
    @Features({"Invalid input"})
    public void enterEmptyExpression() {
        calculateExpression("", "undefined");
    }
}
